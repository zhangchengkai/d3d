cbuffer cbPerObject : register(b0)
{
	float4x4 gWorldViewProj;
	float gTime;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
	float4 Color : COLOR;
};

float4 main(VertexOut pin) : SV_TARGET
{
	pin.Color.rgb *= 0.7f + 0.25f*sin(2.5f*gTime);
	return pin.Color;
}