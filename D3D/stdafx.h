// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <windowsx.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
#include <d3d12.h>
#include <dxgi1_6.h>
#include <wrl\client.h>
#include "d3dx12.h"
#include "Helpers.h"
#include <string>
#include <DirectXColors.h>
#include <vector>
#include <array>
#include <d3dcompiler.h>
#include <fstream>
#include <unordered_map>

#undef _countof
#define _countof(x) sizeof(x)/sizeof(*(x))